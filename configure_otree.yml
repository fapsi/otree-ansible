---
- name: setup otree
  become: true
  hosts: all
  tasks:
    # create new project from given directory (in "files/$user") or clone project from given git-repository
    - name: remove old projects
      file:
        state: absent
        path: "{{ home_path }}/{{ item.key }}/otree/"
      loop: "{{ otree_users|dict2items }}"
      when: item.value.override

    - name: add project folder again
      file:
        path: "{{ home_path }}/{{ item.key }}/otree/"
        state: directory
        owner: "{{ item.key }}"
        group: "{{ item.key }}"
        mode: "0700"
      loop: "{{ otree_users|dict2items }}"
      when: item.value.override

    - name: copy over ssh key files
      synchronize:
        src: "keys/{{ item.key }}/"
        dest: "{{ home_path }}/{{ item.key }}/.ssh/"
        delete: yes
      loop: "{{ otree_users|dict2items }}"
      when: "(item.value.override and item.value.app_repo is defined) and ('keys/{{ item.key }}/' is directory)"

    - name: set owner and group permissions restricted for key files
      file:
        path: "{{ home_path }}/{{ item.key }}/.ssh/"
        state: directory
        owner: "{{ item.key }}"
        group: "{{ item.key }}"
        mode: "0600"
        recurse: yes
      loop: "{{ otree_users|dict2items }}"
      when: item.value.override and item.value.app_repo is defined

    - name: set owner and group permissions for folder
      file:
        path: "{{ home_path }}/{{ item.key }}/.ssh/"
        state: directory
        owner: "{{ item.key }}"
        group: "{{ item.key }}"
        mode: "0700"
      loop: "{{ otree_users|dict2items }}"
      when: item.value.override and item.value.app_repo is defined

    - name: clone projects accordingly
      git:
        repo: "{{ item.value.app_repo }}"
        dest: "{{ home_path }}/{{ item.key }}/otree/"
        accept_hostkey: yes
        recursive: yes
        key_file: "{% if (home_path + '/' + item.key + '/.ssh/id') is file %}{{ home_path }}/{{ item.key }}/.ssh/id{% endif %}"
      become: yes
      become_user: "{{ item.key }}"
      loop: "{{ otree_users|dict2items }}"
      when: item.value.override and item.value.app_repo is defined

    - name: copy over project files
      synchronize:
        src: "files/{{ item.key }}/"
        dest: "{{ home_path }}/{{ item.key }}/otree/"
        delete: yes
      loop: "{{ otree_users|dict2items }}"
      when: item.value.override and item.value.app_repo is not defined

    - name: set owner and group permissions
      file: 
        path: "{{ home_path }}/{{ item.key }}/otree/"
        state: directory
        owner: "{{ item.key }}"
        group: "{{ item.key }}"
        mode: "0700"
      loop: "{{ otree_users|dict2items }}"
      when: item.value.override and item.value.app_repo is not defined

    # clear old venv directory since python.X -m venv --clear is only executed once on creation 
    - name: remove old venv
      file:
        state: absent
        path: "{{ home_path }}/{{ item.key }}/.venv_otree/"
      become: yes
      become_user: "{{ item.key }}"
      loop: "{{ otree_users|dict2items }}"
      when: item.value.override


    # install python dependencies and
    - name: install psycopg2
      pip:
        name: psycopg2
        state: present
        virtualenv: "{{ home_path }}/{{ item.key }}/.venv_otree"
        virtualenv_command: "{{ item.value.python_binary_path }} -m venv"
      become: yes
      become_user: "{{ item.key }}"
      loop: "{{ otree_users|dict2items }}"
      when: item.value.override

    # no need to install otree explicitly since otree should be in requirements of your otree project
    #- name: install otree
    #  pip:
    #    name: otree
    #    state: present
    #    extra_args: "{{ '--pre' if item.value.pre_release else '' }}"
    #    virtualenv: "{{ home_path }}/{{ item.key }}/.venv_otree"
    #    virtualenv_command: "/usr/bin/python{{ '3.7' if item.value.pre_release else '3'}} -m venv"
    #  become: yes
    #  become_user: "{{ item.key }}"
    #  loop: "{{ otree_users|dict2items }}"
    #  when: item.value.override

    - name: install requirements if exists (usually contains otree as requirement; make sure pinning the version)
      pip:
        state: present
        requirements: "{{ home_path }}/{{ item.key }}/otree/requirements.txt"
        virtualenv: "{{ home_path }}/{{ item.key }}/.venv_otree"
        virtualenv_command: "{{ item.value.python_binary_path }} -m venv"
      become: yes
      become_user: "{{ item.key }}"
      loop: "{{ otree_users|dict2items }}"
      when: item.value.override
      #ignore_errors: yes

    # reset and prepare otree
    - name: reset otree database
      shell: "source {{ home_path }}/{{ item.key }}/.venv_otree/bin/activate && otree resetdb --noinput"
      args:
        chdir: "{{ home_path }}/{{ item.key }}/otree/"
        executable: /bin/bash
      environment:
        DATABASE_URL: "postgres://{{ item.key }}:{{ item.value.admin_pw }}@localhost:5432/instance_{{ item.key }}"
        REDIS_URL: "{{ item.value.redis }}"
        OTREE_ADMIN_PASSWORD: "{{ item.value.admin_pw }}"
        OTREE_AUTH_LEVEL: "{{ item.value.auth_level }}"
        OTREE_SECRET_KEY: "{{ item.value.secret_key }}"
        OTREE_PRODUCTION: "{{ item.value.production }}"
        PORT: "{{ item.value.port }}"
      become: yes
      become_user: "{{ item.key }}"
      loop: "{{ otree_users|dict2items }}"
      when: item.value.override
